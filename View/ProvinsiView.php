<?php

namespace View {

    use Service\ProvinsiService;
    use Helper\InputHelper;

    class ProvinsiView
    {

        private ProvinsiService $provinsiService;

        public function __construct(ProvinsiService $provinsiService)
        {
            $this->provinsiService = $provinsiService;
        }

        function showProvinsi(): void
        {
            while (true) {
                $this->provinsiService->showProvinsi();

                echo "MENU" . PHP_EOL;
                echo "1. Tambah Provinsi" . PHP_EOL;
                echo "2. Hapus Provinsi" . PHP_EOL;
                echo "3. Update Provinsi" . PHP_EOL; // Tambahkan opsi update
                echo "4. Lihat Detail Provinsi" . PHP_EOL;
                echo "x. Keluar" . PHP_EOL;

                $pilihan = InputHelper::input("Pilih");

                if ($pilihan == "1") {
                    $this->addProvinsi();
                } else if ($pilihan == "2") {
                    $this->removeProvinsi();
                } else if ($pilihan == "3") { // Panggil metode updateProvinsi
                    $this->updateProvinsi();
                } else if ($pilihan == "4") {
                    $this->viewDetailProvinsi();
                } else if ($pilihan == "x") {
                    break;
                } else {
                    echo "Pilihan tidak dimengerti" . PHP_EOL;
                }
            }

            echo "Sampai Jumpa Lagi" . PHP_EOL;
        }

        function addProvinsi(): void
        {
            echo "MENAMBAH PROVINSI" . PHP_EOL;

            $nama_provinsi = InputHelper::input("Nama Provinsi (x untuk batal)");
            if ($nama_provinsi == "x") {
                echo "Batal menambah Provinsi" . PHP_EOL;
                return;
            }

            $tahun_berdiri = InputHelper::input("Tahun Berdiri");
            $gubernur = InputHelper::input("Gubernur");

            $this->provinsiService->addProvinsi($nama_provinsi, $tahun_berdiri, $gubernur);
        }

        function removeProvinsi(): void
        {
            echo "MENGHAPUS PROVINSI" . PHP_EOL;

            $id = InputHelper::input("ID Provinsi (x untuk batalkan)");

            if ($id == "x") {
                echo "Batal menghapus provinsi" . PHP_EOL;
                return;
            }

            $this->provinsiService->removeProvinsi((int)$id);
        }

        function updateProvinsi(): void
        {
            echo "MENGUPDATE PROVINSI" . PHP_EOL;

            $id = InputHelper::input("ID Provinsi untuk Update (x untuk batalkan)");

            if ($id == "x") {
                echo "Batal mengupdate provinsi" . PHP_EOL;
                return;
            }

            $existingProvinsi = $this->provinsiService->getProvinsiById((int)$id);

            if (!$existingProvinsi) {
                echo "ID Provinsi tidak valid." . PHP_EOL;
                return;
            }

            // Meminta input dari pengguna untuk data yang baru
            $nama_provinsi = InputHelper::input("Nama Provinsi Baru (" . $existingProvinsi->getNama_provinsi() . ")");
            $tahun_berdiri = InputHelper::input("Tahun Berdiri Baru (" . $existingProvinsi->getTahunBerdiri() . ")");
            $gubernur = InputHelper::input("Gubernur Baru (" . $existingProvinsi->getGubernur() . ")");

            // Memeriksa apakah pengguna mengosongkan input untuk data yang baru
            if (empty($nama_provinsi)) {
                $nama_provinsi = $existingProvinsi->getNama_provinsi();
            }
            if (empty($tahun_berdiri)) {
                $tahun_berdiri = $existingProvinsi->getTahunBerdiri();
            }
            if (empty($gubernur)) {
                $gubernur = $existingProvinsi->getGubernur();
            }

            // Buat array asosiatif dengan data yang diperbarui
            $updatedData = [
                'nama_provinsi' => $nama_provinsi,
                'tahun_berdiri' => $tahun_berdiri,
                'gubernur' => $gubernur,
            ];

            // Panggil metode updateProvinsi pada servis dengan ID Provinsi dan data yang diperbarui
            $this->provinsiService->updateProvinsi((int)$id, $updatedData);

            echo "Data Provinsi berhasil diupdate." . PHP_EOL;
        }

        function viewDetailProvinsi(): void
        {
            $id = InputHelper::input("ID Provinsi untuk Lihat Detail");
            $this->provinsiService->viewDetailProvinsi((int)$id);
        }
    }
}
