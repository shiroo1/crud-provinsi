<?php

require_once __DIR__ . "/../Entity/Umkm.php";
require_once __DIR__ . "/../Repository/UmkmRepository.php";
require_once __DIR__ . "/../Service/UmkmService.php";
require_once __DIR__ . '/../View/UmkmView.php';
require_once __DIR__ . '/../Helper/InputHelper.php';

use \Entity\Umkm;
use \Repository\UmkmRepositoryImpl;
use \Service\UmkmServiceImpl;
use \View\UmkmView;

function testViewShowUmkm(): void
{

    $umkmRepository = new UmkmRepositoryImpl();
    $umkmService = new UmkmServiceImpl($umkmRepository);
    $UmkmView = new UmkmView($umkmService );

    $umkmService ->addUmkm("Donat Pak Ewing");

    $UmkmView->showUmkm();
}

function testViewAddUmkm(): void
{

    $umkmRepository = new UmkmRepositoryImpl();
    $umkmService  = new UmkmServiceImpl($umkmRepository);
    $UmkmView = new UmkmView($umkmService );

    $umkmService ->addUmkm("Donat Pak Ewing");

    $umkmService ->showUmkm();

    $UmkmView->addUmkm();

    $umkmService ->showUmkm();

    $UmkmView->addUmkm();

    $umkmService ->showUmkm();
}

function testViewRemoveUmkm(): void
{

    $umkmRepository = new UmkmRepositoryImpl();
    $umkmService  = new UmkmServiceImpl($umkmRepository);
    $UmkmView = new UmkmView($umkmService );

    $umkmService ->addUmkm("Donat Pak Ewing");

    $umkmService ->showUmkm();

    $UmkmView->removeUmkm();

    $umkmService ->showUmkm();

    $UmkmView->removeUmkm();

    $umkmService ->showUmkm();
}

testViewRemoveUmkm();
