<?php

require_once __DIR__ . "/../Entity/Umkm.php";
require_once __DIR__ . "/../Repository/UmkmRepository.php";
require_once __DIR__ . "/../Service/UmkmService.php";
require_once __DIR__ . "/../Config/Database.php";

use Entity\Umkm;
use Service\UmkmServiceImpl;
use Repository\UmkmRepositoryImpl;

function testShowUmkm(): void
{
    $connection = \Config\Database::getConnection();
    $todolistRepository = new UmkmRepositoryImpl($connection);
    $todolistService = new UmkmServiceImpl($todolistRepository);

    $todolistService->showUmkm();
}

function testaddUmkm(): void
{
    $connection = \Config\Database::getConnection();
    $todolistRepository = new UmkmRepositoryImpl($connection);

    $todolistService = new UmkmServiceImpl($todolistRepository);
    $todolistService->addUmkm("Donat");
}

function testRemoveTodolist(): void
{
    $connection = \Config\Database::getConnection();
    $todolistRepository = new UmkmRepositoryImpl($connection);

    $todolistService = new UmkmServiceImpl($todolistRepository);

    echo $todolistService->removeUmkm(5) . PHP_EOL;
    echo $todolistService->removeUmkm(4) . PHP_EOL;
    echo $todolistService->removeUmkm(3) . PHP_EOL;
    echo $todolistService->removeUmkm(2) . PHP_EOL;
    echo $todolistService->removeUmkm(1) . PHP_EOL;
}

testShowUmkm();
