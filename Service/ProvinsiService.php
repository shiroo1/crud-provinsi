<?php

namespace Service {

    use Entity\Provinsi;
    use Repository\ProvinsiRepository;

    interface ProvinsiService
    {
        function showProvinsi(): void;
        function addProvinsi(string $nama_provinsi, string $tahun_berdiri, string $gubernur): void;
        function removeProvinsi(int $id): void;
        function viewDetailProvinsi(int $id): void;
        function updateProvinsi(int $id, array $updatedData): void;
    }

    class ProvinsiServiceImpl implements ProvinsiService
    {
        private ProvinsiRepository $provinsiRepository;

        public function __construct(ProvinsiRepository $provinsiRepository)
        {
            $this->provinsiRepository = $provinsiRepository;
        }

        function showProvinsi(): void
        {
            echo "List Provinsi Di Indonesia" . PHP_EOL;
            $provinsiList = $this->provinsiRepository->findAll();
            foreach ($provinsiList as $provinsi) {
                echo $provinsi->getId() . ". " . $provinsi->getNama_provinsi() . PHP_EOL;
            }
        }

        function addProvinsi(string $nama_provinsi, string $tahun_berdiri, string $gubernur): void
        {
            // Validasi input
            if (empty($nama_provinsi) || empty($tahun_berdiri) || empty($gubernur)) {
                echo "Data Provinsi tidak lengkap. Semua field harus diisi." . PHP_EOL;
                return;
            }

            $provinsi = new Provinsi();
            $provinsi->setNama_provinsi($nama_provinsi);
            $provinsi->setTahunBerdiri($tahun_berdiri);
            $provinsi->setGubernur($gubernur);

            $this->provinsiRepository->save($provinsi);
            echo "SUKSES MENAMBAH PROVINSI" . PHP_EOL;
        }

        function removeProvinsi(int $id): void
        {
            // Validasi ID Provinsi
            $provinsi = $this->provinsiRepository->findById($id);
            if (!$provinsi) {
                echo "ID Provinsi tidak valid." . PHP_EOL;
                return;
            }

            if ($this->provinsiRepository->remove($id)) {
                echo "SUKSES MENGHAPUS PROVINSI" . PHP_EOL;
            } else {
                echo "GAGAL MENGHAPUS PROVINSI" . PHP_EOL;
            }
        }

        function viewDetailProvinsi(int $id): void
        {
            // Validasi ID Provinsi
            $provinsi = $this->provinsiRepository->findById($id);
            if (!$provinsi) {
                echo "ID Provinsi tidak valid." . PHP_EOL;
                return;
            }

            $separator = str_repeat("=", 50); // Buat garis pemisah panjang

            echo $separator . PHP_EOL;
            echo "Detail Provinsi" . PHP_EOL;
            echo $separator . PHP_EOL;
            echo "Nama Provinsi: " . $provinsi->getNama_provinsi() . PHP_EOL;
            echo "Gubernur: " . $provinsi->getGubernur() . PHP_EOL;
            echo "Tahun Berdiri: " . $provinsi->getTahunBerdiri() . PHP_EOL;
            echo $separator . PHP_EOL;
        }

        function updateProvinsi(int $id, array $updatedData): void
        {
            // Validasi ID Provinsi
            $provinsi = $this->provinsiRepository->findById($id);
            if (!$provinsi) {
                echo "ID Provinsi tidak valid." . PHP_EOL;
                return;
            }

            // Membuat objek Provinsi baru dengan data yang diperbarui
            $updatedProvinsi = new Provinsi();
            $updatedProvinsi->setId($id);
            $updatedProvinsi->setNama_provinsi($updatedData['nama_provinsi'] ?? $provinsi->getNama_provinsi());
            $updatedProvinsi->setTahunBerdiri($updatedData['tahun_berdiri'] ?? $provinsi->getTahunBerdiri());
            $updatedProvinsi->setGubernur($updatedData['gubernur'] ?? $provinsi->getGubernur());

            if ($this->provinsiRepository->update($updatedProvinsi)) {
                echo "SUKSES MENGUPDATE PROVINSI" . PHP_EOL;
            } else {
                echo "GAGAL MENGUPDATE PROVINSI" . PHP_EOL;
            }
        }

        function getProvinsiById(int $id): ?Provinsi
        {
            return $this->provinsiRepository->findById($id);
        }
    }
}
