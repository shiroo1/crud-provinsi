<?php

namespace Entity {

    class Provinsi
    {
        private int $id;
        private string $nama_provinsi;
        private string $tahun_berdiri; // Menjaga kolom "tahun_berdiri"
        private string $gubernur; // Menambahkan kolom "gubernur"

        public function __construct(string $nama_provinsi = "", string $tahun_berdiri = "", string $gubernur = "")
        {
            $this->nama_provinsi = $nama_provinsi;
            $this->tahun_berdiri = $tahun_berdiri;
            $this->gubernur = $gubernur;
        }

        public function setId(int $id): void
        {
            $this->id = $id;
        }

        public function getId(): int
        {
            return $this->id;
        }

        public function getNama_provinsi(): string
        {
            return $this->nama_provinsi;
        }

        public function setNama_provinsi(string $nama_provinsi): void
        {
            $this->nama_provinsi = $nama_provinsi;
        }

        public function getTahunBerdiri(): string
        {
            return $this->tahun_berdiri;
        }

        public function setTahunBerdiri(string $tahun_berdiri): void
        {
            $this->tahun_berdiri = $tahun_berdiri;
        }

        public function getGubernur(): string
        {
            return $this->gubernur;
        }

        public function setGubernur(string $gubernur): void
        {
            $this->gubernur = $gubernur;
        }
    }
}
