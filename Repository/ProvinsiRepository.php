<?php

namespace Repository {

    use Entity\Provinsi;

    interface ProvinsiRepository
    {
        function save(Provinsi $provinsi): void;
        function remove(int $id): bool;
        function findAll(): array;
        function findById(int $id): ?Provinsi;
        function update(Provinsi $provinsi): bool;
    }

    class ProvinsiRepositoryImpl implements ProvinsiRepository
    {
        private \PDO $connection;

        public function __construct(\PDO $connection)
        {
            $this->connection = $connection;
        }

        function save(Provinsi $provinsi): void
        {
            $sql = "INSERT INTO tb_provinsi(nama_provinsi, tahun_berdiri, gubernur) VALUES (?, ?, ?)";
            $statement = $this->connection->prepare($sql);
            $statement->execute([
                $provinsi->getNama_provinsi(),
                $provinsi->getTahunBerdiri(),
                $provinsi->getGubernur()
            ]);
        }

        function remove(int $id): bool
        {
            $sql = "DELETE FROM tb_provinsi WHERE id = ?";
            $statement = $this->connection->prepare($sql);
            $statement->execute([$id]);

            return $statement->rowCount() > 0;
        }

        function findAll(): array
        {
            $sql = "SELECT id, nama_provinsi, tahun_berdiri, gubernur FROM tb_provinsi";
            $statement = $this->connection->prepare($sql);
            $statement->execute();

            $result = [];

            foreach ($statement as $row) {
                $provinsi = new Provinsi();
                $provinsi->setId($row['id']);
                $provinsi->setNama_provinsi($row['nama_provinsi']);
                $provinsi->setTahunBerdiri($row['tahun_berdiri']);
                $provinsi->setGubernur($row['gubernur']);

                $result[] = $provinsi;
            }

            return $result;
        }

        function findById(int $id): ?Provinsi
        {
            $sql = "SELECT id, nama_provinsi, tahun_berdiri, gubernur FROM tb_provinsi WHERE id = ?";
            $statement = $this->connection->prepare($sql);
            $statement->execute([$id]);

            $row = $statement->fetch(\PDO::FETCH_ASSOC);

            if (!$row) {
                return null;
            }

            $provinsi = new Provinsi();
            $provinsi->setId($row['id']);
            $provinsi->setNama_provinsi($row['nama_provinsi']);
            $provinsi->setTahunBerdiri($row['tahun_berdiri']);
            $provinsi->setGubernur($row['gubernur']);

            return $provinsi;
        }

        function update(Provinsi $provinsi): bool
        {
            $sql = "UPDATE tb_provinsi SET nama_provinsi = ?, tahun_berdiri = ?, gubernur = ? WHERE id = ?";
            $statement = $this->connection->prepare($sql);
            $result = $statement->execute([
                $provinsi->getNama_provinsi(),
                $provinsi->getTahunBerdiri(),
                $provinsi->getGubernur(),
                $provinsi->getId(),
            ]);

            return $result;
        }
    }
}
