<?php

require_once __DIR__ . '/Entity/provinsi.php';
require_once __DIR__ . '/Helper/InputHelper.php';
require_once __DIR__ . '/Repository/ProvinsiRepository.php';
require_once __DIR__ . '/Service/ProvinsiService.php';
require_once __DIR__ . '/View/ProvinsiView.php';
require_once __DIR__ . '/Config/Database.php';

use Repository\ProvinsiRepositoryImpl;
use Service\ProvinsiServiceImpl;
use View\ProvinsiView;

echo "Aplikasi Pendataan Provinsi" . PHP_EOL;

$connection = \Config\Database::getConnection();
$provinsiRepository = new ProvinsiRepositoryImpl($connection);
$provinsiService = new ProvinsiServiceImpl($provinsiRepository);
$provinsiView = new ProvinsiView($provinsiService);

$provinsiView->showProvinsi();
